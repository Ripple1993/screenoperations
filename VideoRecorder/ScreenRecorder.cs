﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Video.FFMPEG;

namespace VideoRecorder
{
    class ScreenRecorder
    {
        // =======================================================================
        //1.Install package ffmpeg with nuGetPackage
        //2. Antes de definir nosso construtor, precisamos de algumas variáveis. 

        //Video Variables
        private Rectangle bounds; //Bordas da tela que serão salvas.
        private string outputPath = ""; //Onde o video final ficará localizado.
        private string tempPath = ""; //Criaremos uma pasta temporária e jogaremos vários screenshots nessa pasta e apos terminar, juntaremos os screenshots e o audio.
        private int fileCount = 1; //Dar um identificador à cada screenshot
        private List<string> inputFrameSequence = new List<string>(); //Armazenar os nomes de todos os screenshots 

        //File Variables
        private string audioName = "audio.wav";
        private string videoName = "video.mp4";
        private string fileName = "Record.mp4";

        //Time Variable
        Stopwatch stopWatch = new Stopwatch(); //vairável de tempo de gravação
        private Rectangle rectangle;
        private string v;

        //Audio Variables
        public static class NativeMethods //Método para pegar o audio
        {
            [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
            public static extern int record(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
        }


        //Construtor com 2 parametros 
        public ScreenRecorder(Rectangle b, string outPath)
        {
            //Irá criar a pasta temporária chamada tempScreenshots que irá armazenar todos os screenshots.
            CreateTempFolder("tempScreenshots");
            bounds = b;
            outputPath = outPath;
        }
        //Métodos para criar pasta temporária
        private void CreateTempFolder(string name)
        {
            if (Directory.Exists("D://"))
            {
                string pathName = $"D://{name}";
                Directory.CreateDirectory(pathName);
                tempPath = pathName;
            }
            else
            {
                string pathName = $"C://{name}";
                Directory.CreateDirectory(pathName);
                tempPath = pathName;
            }
        }


        //Método que irá deletar todos os screenshots e os direórios.
        private void DeletePath(string targetDir)
        {
            string[] files = Directory.GetFiles(targetDir);
            string[] dirs = Directory.GetDirectories(targetDir);
            foreach (string file in files)
            {
                //Temos que fazer isso para deletar o arquivo. Porque o arquivo é do tipo read only.
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }
            foreach (string item in dirs)
            {
                DeletePath(item);
            }
            Directory.Delete(targetDir, false);
        }


        //Método que irá deletar o arquivo de áudio e o arquivo gerado a partir da junção de todos os screenshots, porém irá manter o arquivo merge entre vídeo e audio.
        private void DeleteFilesExcept(string targetFile, string excFile)
        {
            string[] files = Directory.GetFiles(targetFile);
            foreach (string file in files)
            {
                if (file != excFile)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }
            }
        }
        //Se o usuário fechar a aplicação ou se o programa der problema, este método irá deletar os screenshots.
        public void Cleanup()
        {
            if (Directory.Exists(tempPath))
            {
                DeletePath(tempPath);
            }
        }
        //Vamos começar a mexer em coisas relacionadas ao video. 
        //Irá retornar o tempo que levou de gravação 
        //public string getElapsed()
        //{
        //   
        //}
        public void RecordVideo()
        {
            //vamos usar um bitmap para criar um screenshot da tela durante a iteração do void. Assim que tivermos o screenshot precisamos salva-lo. 
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                }
                //salvando o screenshot
                string name = tempPath + "//screenshot-" + fileCount + ".png";
                bitmap.Save(name, ImageFormat.Png);
                inputFrameSequence.Add(name);
                fileCount += 1;
                bitmap.Dispose();
            }
        }
        public void RecordAudio()
        {
            NativeMethods.record("open new Type waveaudio Alias recsound", "", 0, 0);
            NativeMethods.record("record recsound", "", 0, 0);
        }

        //Adicionar screenshots ao vídeo.
        private void SaveVideo(int width, int height, int frameRate)
        {
            using (VideoFileWriter vFWriter = new VideoFileWriter())
            {
                vFWriter.Open(outputPath + "//" + videoName, width, height, frameRate, VideoCodec.MPEG4);
                foreach (string imageLoc in inputFrameSequence)
                {
                    Bitmap imageFrame = System.Drawing.Image.FromFile(imageLoc) as Bitmap;
                    vFWriter.WriteVideoFrame(imageFrame);
                    imageFrame.Dispose();
                }
                vFWriter.Close();
            }
        }
        private void SaveAudio()
        {
            string audioPath = "save recsound" + outputPath + "//" + audioName;
            NativeMethods.record(audioPath, "", 0, 0);
            NativeMethods.record("close recsound", "", 0, 0);
        }
        //Aqui criaremos um command prompt temporário e iremos executar o ffmpeg command neste prompt. 
        private void CombineVideoAudio(string video, string audio)
        {
            string command = $"/c ffmpeg -i \"{video}\" -i \"{audio}\" -shortest {fileName}";
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                CreateNoWindow = false,
                FileName = "cmd.exe",
                WorkingDirectory = outputPath,
                Arguments = command
            };
            using (Process exeProcess = Process.Start(startInfo))
            {
                exeProcess.WaitForExit();
            }
        }
        public void Stop()
        {
            int width = bounds.Width;
            int height = bounds.Height;
            int frameRate = 10;
            SaveAudio();
            SaveVideo(width, height, frameRate);
            CombineVideoAudio(videoName, audioName);
            DeletePath(tempPath);
            //DeleteFilesExcept(outputPath, outputPath + "\\" + fileName);
        }
    }
}
