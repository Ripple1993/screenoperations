﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Video.FFMPEG;

namespace VideoRecorder
{
    public partial class Form1 : Form
    {
        bool folderSelected = false;
        string outputPath = "";
        string finalVideoName = "FinalVideo.mp4";
        ScreenRecorder screenRec = new ScreenRecorder(new Rectangle(),"");


        public Form1()
        {
            InitializeComponent();
        }
        

        private void BtnSelectFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.Description = "Select an Output Folder";
            if(folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                outputPath = folderBrowser.SelectedPath;
                folderSelected = true;
                Rectangle bounds = Screen.FromControl(this).Bounds;
                screenRec = new ScreenRecorder(bounds, outputPath);
            }
            else
            {
                MessageBox.Show("Please select a folder...","Error");
            }           
        }

        private void TmrRecord_Tick(object sender, EventArgs e)
        {
            screenRec.RecordAudio();
            screenRec.RecordVideo();
            
        }

        private void BtnRecord_Click(object sender, EventArgs e)
        {
            if (folderSelected)
            {
                tmrRecord.Start();
            }
            else
            {
                MessageBox.Show("You must select an output folder before recording", "Error");
            }
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            tmrRecord.Stop();
            screenRec.Stop();
        }
    }
}
