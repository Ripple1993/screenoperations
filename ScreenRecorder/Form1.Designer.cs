﻿namespace ScreenRecorder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pctBoxImage = new System.Windows.Forms.PictureBox();
            this.btnCaptureImage = new System.Windows.Forms.Button();
            this.btnCaptureVideo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pctBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // pctBoxImage
            // 
            this.pctBoxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pctBoxImage.Location = new System.Drawing.Point(13, 50);
            this.pctBoxImage.Name = "pctBoxImage";
            this.pctBoxImage.Size = new System.Drawing.Size(739, 388);
            this.pctBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctBoxImage.TabIndex = 0;
            this.pctBoxImage.TabStop = false;
            this.pctBoxImage.UseWaitCursor = true;
            // 
            // btnCaptureImage
            // 
            this.btnCaptureImage.BackColor = System.Drawing.Color.Black;
            this.btnCaptureImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCaptureImage.ForeColor = System.Drawing.Color.White;
            this.btnCaptureImage.Location = new System.Drawing.Point(13, 12);
            this.btnCaptureImage.Name = "btnCaptureImage";
            this.btnCaptureImage.Size = new System.Drawing.Size(103, 32);
            this.btnCaptureImage.TabIndex = 1;
            this.btnCaptureImage.Text = "Capturar Tela";
            this.btnCaptureImage.UseVisualStyleBackColor = false;
            this.btnCaptureImage.UseWaitCursor = true;
            this.btnCaptureImage.Click += new System.EventHandler(this.btnCaptureScreen_Click);
            // 
            // btnCaptureVideo
            // 
            this.btnCaptureVideo.BackColor = System.Drawing.Color.Black;
            this.btnCaptureVideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCaptureVideo.ForeColor = System.Drawing.Color.White;
            this.btnCaptureVideo.Location = new System.Drawing.Point(122, 12);
            this.btnCaptureVideo.Name = "btnCaptureVideo";
            this.btnCaptureVideo.Size = new System.Drawing.Size(111, 32);
            this.btnCaptureVideo.TabIndex = 2;
            this.btnCaptureVideo.Text = "Capturar Video";
            this.btnCaptureVideo.UseVisualStyleBackColor = false;
            this.btnCaptureVideo.UseWaitCursor = true;
            this.btnCaptureVideo.Click += new System.EventHandler(this.btnCaptureVideo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(764, 450);
            this.Controls.Add(this.btnCaptureVideo);
            this.Controls.Add(this.btnCaptureImage);
            this.Controls.Add(this.pctBoxImage);
            this.Name = "Form1";
            this.Text = "Captura de Tela";
            this.UseWaitCursor = true;
            ((System.ComponentModel.ISupportInitialize)(this.pctBoxImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pctBoxImage;
        private System.Windows.Forms.Button btnCaptureImage;
        private System.Windows.Forms.Button btnCaptureVideo;
    }
}

