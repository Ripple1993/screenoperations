﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;

namespace ScreenRecorder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Userinterface buttons methods
        private void btnCaptureScreen_Click(object sender, EventArgs e)
        {
            Thread thrd = new Thread(CaptureScreen);
            thrd.Start();
        }
        private void btnCaptureVideo_Click(object sender, EventArgs e)
        {
            Thread thrd = new Thread(CaptureScreenVideo);
            thrd.Start();
        }

        //Methods
        private void CaptureScreen()
        {
            Bitmap bm = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics g = Graphics.FromImage(bm);
            g.CopyFromScreen(0, 0, 0, 0, bm.Size);
            pctBoxImage.Image = bm;
        }
        private void CaptureScreenVideo()
        {
            while (true)
            {
                Bitmap bm = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
                Graphics g = Graphics.FromImage(bm);
                g.CopyFromScreen(0, 0, 0, 0, bm.Size);
                pctBoxImage.Image = bm;
                Thread.Sleep(50);
            }
        }

        
    }
}
